# README
## Commands

To run the tests:

    ./mvnw compile test

To compile and package the project:

    ./mvnw compile package
    
To run the project, find the challenge-1.0-SNAPSHOT.jar and run:

    java -jar challenge-1.0-SNAPSHOT.jar server configuration.yml

#### Notes
A compiled jar for the project is available in the root folder.
## Using the API
  ### Query an account using its id
  To query the account with id: {id} use GET: http://localhost:8080/v1/api/accounts/{id}
  
  curl command:
  
      curl http://localhost:8080/v1/api/accounts/{id}
  
  ### Create an account with balance
  To create a new account with balance:{balance} use POST: http://localhost:8080/v1/api/accounts/
  with the following json object:
  
     {
     
       "id":null,
       
       "balance": {balance}
       
     }

  curl command:
    
      curl -d '{"id":null,"balance": {balance}}' -H "Content-Type: application/json" -X POST http://localhost:8080/v1/api/accounts/
  
  ### Deposit money into an account
  To modify an existing account with {id} and deposit an amount of {amount} use PUT:
  http://localhost:8080/v1/api/accounts/{id}/deposit/{amount}

  curl command:
      
    curl PUT http://localhost:8080/v1/api/accounts/{id}/deposit/{amount}
 
  ### Create a transfer
  To create a transfer with amount: {amount} from account: {from} to account {to} and reference 
  {reference} use POST: http://localhost:8080/v1/api/transfers/
  with the following json object:
      
    {
        "id":null,
        "fromId": {from},
        "toId": {to},
        "amount":{amount},
        "reference":{reference}
    }
      
  curl command:
    
    curl -d '{"id":null,"fromId": {from},"toId": {to},"amount":{amount},"reference":{reference}}' -H "Content-Type: application/json" -X POST http://localhost:8080/v1/api/transfers/
## Documentation
 ### configuration package
 this package contains the classes that help stock information:
  #### RepositoryContext class
  Contains a collection of repositories, in this case different context means different data store.
  Every information that is useful about the repositories (database link, driver) should be stored
  in that class.
  #### TransferConfiguration class
  This class extends the Dropwizard Configuration class. It contains the data that is stored in the 
  configuration.yml file
 ### domain package
 This package contains models used in the service.
  #### Model
  Base class for every data that has to be stored in the data stores. It contains an id (Long).
  It implements the basic querying functions such as get(), create(), and update(). To enable
  advance querying, It also contains a link to the repositories.
  #### Account
   This class extends the Model class. it contains the structure of an account:
   * balance : Long
   * inbound : List<Transfer> list of transfer going in the account
   * outbound : List<Transfer> list of transfer going out of the account
   It has function such as deposit, withdraw, addInbound, and addOutbound which are synchronized 
   to enable concurrent modification on the balance, outbound and inbound fields(multiple transfers).
  #### Transfer
   This class extends the Model class. it contains the structure of an transfer:
   * fromId : Long
   * toId : Long
   * amount : Long
   * reference : String
   * timestamp : Instant
   
   From and to id can be external (iban, account number or sort code). But for this example, to
   actually transfer money between account, it has to match an account id in the system
   
 ### exceptions package
 This package contains all the custom exceptions that are thrown by our system
  #### EntityContainsIdException
  This class extends the ResourceException class. This exception Thrown when an entity scheduled to 
  be created already has an id, id should be generated 
  by a sequence in the system
  #### EntityNotFoundException
  This class extends the ResourceException class. This exception thrown when the search for an 
  entity has failed
  #### FailedToMapException
  This class extends the ResourceException class. This exception thrown when a json mapping has 
  failed in the resources
  #### ForeignKeyNotFoundException
  This class extends the ResourceException class. This exception thrown when a foreign key 
  constraint is violated
  #### NotEnoughFundsException
  This class extends the ResourceException class. This exception thrown when there are not enough 
  funds in the account in a withdrawal
  #### RepositoryNotFoundException
  This class extends the ResourceException class. This exception thrown when the repository for 
  a certain model is not registered in the context
  #### ResourceException
  This class extends Throwable. It the parent class all the exception that will be thrown in a resource.
 ### exceptions.mappers package
 This package contains the mapper that will be registered in the jersey environment.
  #### ResourceExceptionMapper
  This class is a mapper that maps a Resource Exception to a JsonObject.
 ### healthchecks package
 This package contains all the health checks of the application.
  #### DummyHealthCheck
  Dummy health check, it does not do anything. It is just here as a placeholder
 ### repositories package
 This package contains the repositories classes.
  #### Repository
  This class contains the basic implementation of a repository:
  * get
  * create
  * update
  It also contains a data store (in memory data store) and implements the ID sequence.
  #### AccountRepository
  This class extends the Repository class using Account as Model. By extending the repository class 
  it creates the basic querying actions for the Account model. All model specific querying actions
  should be done in this class.
  #### TransferRepository
  This class extends the Repository class using Transfer as Model. By extending the repository class 
  it creates the basic querying actions for the Transfer model. All model specific querying actions 
  should be done in this class.
 ### resources package 
 Contains the REST api resources of the applications
  #### AccountResource
  Contains the API that directly modifies the accounts. This class opens the http interface to:
  * Create an account
  * Deposit money in the account
  * Get an account
  #### TransferResource
  Provides an http interface for the application's health checks
  #### TransferResource
  Contains the API that creates a transfer and modifies the accounts. This class opens the http 
  interface to:
  * Transfer money from one account to another
 ### serialization package
 This package contains all custom serialization class for Jackson
  #### InstantDeserializerImpl
  This class extends the JsonDeserializer class from Jackson. It is used to create an Instant object
   using a String 
 ### root package (com.revolut.challenge)
 This package contains the main class.
  #### TransferApplication
  This class extends the Application class from Dropwizard. TransferApplication is the main class,
  it starts and register the resources, mappers, repositories, health checks, exception mapper, the
  server and any other function it inherits from the Dropwizard Application class

  
  
 
  
   