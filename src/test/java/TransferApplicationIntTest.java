
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.revolut.challenge.TransferApplication;
import com.revolut.challenge.domain.Account;
import com.revolut.challenge.domain.Transfer;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TransferApplicationIntTest {

  private static long MAX_START_TIME_MS = 5000;
  private static String BASE_URL = "http://localhost:8080/v1/api/";
  private static ObjectMapper objectMapper = new ObjectMapper();

  private Account account = new Account(null, 10L);

  /**
   * Starts a thread containing the server
   * @throws InterruptedException
   */
  @BeforeAll
  public static void startServer() throws InterruptedException {
    Server server = new Server();
    server.start();
    Thread.sleep(MAX_START_TIME_MS);
  }

  /**
   * Performance
   * Check if the server can boot in under a certain time
   * @throws UnirestException
   */
  @Test
  public void appStartsInLess() throws UnirestException {
    HttpResponse<JsonNode> response = Unirest.get(BASE_URL + "healthchecks/up").asJson();
    assertEquals(200, response.getStatus());
    assertEquals("up", response.getBody().getObject().getString("status"));
  }

  /**
   * Integration
   * Check if all the path are correctly registered
   * @throws UnirestException
   */
  @Test
  public void pathsCorrectlyRegistered() throws UnirestException {
    ObjectMapper objectMapper
        = new com.fasterxml.jackson.databind.ObjectMapper();

    String account = objectMapper.valueToTree(this.account).toString();
    String transfer = transferString(0L,1L,3L);

    // Execute the requests
    HttpResponse<JsonNode> healthcheck = Unirest.get(BASE_URL + "healthchecks/up")
        .header("Content-Type", "application/json")
        .asJson();
    HttpResponse<JsonNode> createAccount = Unirest.post(BASE_URL + "accounts")
        .header("Content-Type", "application/json")
        .body(account)
        .asJson();
    HttpResponse<JsonNode> depositMoney = Unirest.put(BASE_URL + "accounts/0/deposit/7")
        .header("Content-Type", "application/json")
        .asJson();
    HttpResponse<JsonNode> accountsById = Unirest.get(BASE_URL + "accounts/0")
        .header("Content-Type", "application/json")
        .asJson();
    HttpResponse<JsonNode> transfers = Unirest.post(BASE_URL + "transfers")
        .header("Content-Type", "application/json")
        .body(transfer)
        .asJson();

    // Check if they are found in the system
    assertNotEquals(404, healthcheck.getStatus());
    assertNotEquals(404, createAccount.getStatus());
    assertNotEquals(404, depositMoney.getStatus());
    assertNotEquals(404, accountsById.getStatus());
    assertNotEquals(404, transfers.getStatus());
  }

  /**
   * Test case: Create two accounts, deposit money into the first, transfer money from the first to
   * the second.
   *
   * @throws UnirestException if a request fails
   */
  @Test
  public void functionalityTest() throws UnirestException {
    String account = objectMapper.valueToTree(this.account).toString();

    // Create the accounts
    JSONObject[] accounts = createAccounts(account);
    Long[] ids = {
        accounts[0].getLong("id"),
        accounts[1].getLong("id")
    };

    //Deposit money in the account
    Unirest.put(BASE_URL + "accounts/" + ids[0] + "/deposit/20")
        .header("Content-Type", "application/json")
        .asJson();

    // Refresh the accounts
    accounts = getAccounts(ids);

    // Check that the deposit worked
    assertEquals(10L + 20L, accounts[0].getLong("balance"));

    // Transfer money from account1 to account 2
    JSONObject transferJson = post(BASE_URL + "transfers", transferString(ids[0], ids[1], 10L));

    // Refresh the accounts
    accounts = getAccounts(ids);

    // Make sure that the balance was updated
    assertEquals(20L, accounts[0].getLong("balance"));
    assertEquals(20L, accounts[1].getLong("balance"));

    // Make sure that the transfer is correctly registered in outbounds
    assertTrue(accounts[0].getJSONArray("outbound").length() > 0);
    assertEquals(transferJson.getLong("id"),
        accounts[0].getJSONArray("outbound").getJSONObject(0).getLong("id"));

    // Make sure that the transfer is correctly registered in inbounds
    assertTrue(accounts[1].getJSONArray("inbound").length() > 0);
    assertEquals(transferJson.getLong("id"),
        accounts[1].getJSONArray("inbound").getJSONObject(0).getLong("id"));
  }

  /**
   * Create two accounts using a json representation
   *
   * @param account json representation
   * @return Array of two accounts
   * @throws UnirestException if Json is malformed or request fails
   */
  JSONObject[] createAccounts(String account) throws UnirestException {
    return new JSONObject[]{
        post(BASE_URL + "accounts", account),
        post(BASE_URL + "accounts", account)
    };
  }

  JSONObject[] getAccounts(Long[] ids) throws UnirestException {
    return new JSONObject[]{
        get(BASE_URL + "accounts/" + ids[0]),
        get(BASE_URL + "accounts/" + ids[1])
    };
  }

  /**
   * Create a transfer object
   * @param id1 fromId (outbound)
   * @param id2 toId (inbound)
   * @param amount amount to transfer
   * @return Transfer string representation
   */
  private String transferString(Long id1, Long id2, Long amount) {
    Transfer transfer = new Transfer(null, id1, id2, amount, "reference");
    return objectMapper.valueToTree(transfer).toString();
  }

  /**
   * get an existing and return the json object
   * @param url the url to get
   * @return the json object
   * @throws UnirestException
   */
  private JSONObject get(String url) throws UnirestException {
    return Unirest.get(url)
        .header("Content-Type", "application/json")
        .asJson()
        .getBody()
        .getObject();
  }

  /**
   * Creates a new object using the url and string body
   * @param url the url to post
   * @param body json representation
   * @return the json object
   * @throws UnirestException
   */
  private JSONObject post(String url, String body) throws UnirestException {
    return Unirest.post(url)
        .header("Content-Type", "application/json")
        .body(body)
        .asJson()
        .getBody()
        .getObject();
  }

  /**
   * Creates a TransferApplication server in another thread
   */
  private static class Server extends Thread {

    @Override
    public void run() {
      super.run();
      TransferApplication application = new TransferApplication();
      try {
        application.run("server", "configuration.yml");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
