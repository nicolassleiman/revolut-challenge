package domain;

import com.revolut.challenge.domain.Model;

/**
 * Dummy Model to use in the model implementation
 */
public class ModelImplementationTest extends Model<ModelImplementationTest> {

  ModelImplementationTest(long id) {
    super(id);
  }

  public ModelImplementationTest() {
    super();
  }

}
