package domain;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import com.revolut.challenge.domain.Transfer;
import java.time.Instant;
import java.util.Objects;
import org.junit.jupiter.api.Test;

/**
 * Testing the Transfer Model
 */
 class TransferTest {


  /**
   * Unit
   * Create an transfer using its repository
   */
  @Test
  void creation() {
    Transfer transfer = new Transfer(0L, 1L, 0L, 0L,
        "test", Instant.now());
    assertTrue(transfer.getId().equals(0L)
        && transfer.getFromId().equals(1L)
        && transfer.getToId().equals(0L)
        && transfer.getAmount().equals(0L));
  }

  /**
   * Unit
   * Test the equals function
   */
  @Test
  void structuralEquality() {
    Transfer transfer = new Transfer(0L, 0L, 0L, 0L,
        "test", Instant.now());
    Transfer test = new Transfer(0L, 0L, 0L, 0L,
        "test", transfer.getTimestamp());
    assertEquals(transfer, test);
  }

  /**
   * Unit
   * Test the string representation
   */
  @Test
  void toStringTest() {
    Transfer transfer = new Transfer(0L, 0L, 0L, 0L,
        "test", Instant.now());
    String test = "Transfer{fromId=0, toId=0, amount=0, id=0, reference=test,"
        + " timestamp="+transfer.getTimestamp()+"}";
    assertEquals(transfer.toString(), test);
  }

  /**
   * Unit
   * Test the hashing function
   */
  @Test
  void hashing() {
    Transfer transfer = new Transfer(0L, 0L, 0L, 0L,
        "test", Instant.now());
    int hashcode = Objects
        .hash(transfer.getId(), transfer.getFromId(), transfer.getToId(), transfer.getAmount(),
            transfer.getReference(), transfer.getTimestamp());
    assertEquals(transfer.hashCode(), hashcode);
  }

}
