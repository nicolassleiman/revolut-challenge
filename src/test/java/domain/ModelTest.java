package domain;

import static junit.framework.Assert.fail;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

import com.revolut.challenge.configuration.RepositoryContext;
import com.revolut.challenge.domain.Model;
import com.revolut.challenge.exceptions.RepositoryNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import repository.ModelImplementationRepositoryTest;

/**
 * Testing the model class
 */
class ModelTest {

  private static RepositoryContext repositoryContext;

  /**
   * Creates a context
   */
  @BeforeAll
  static void createContext() {
    repositoryContext = new RepositoryContext();
    Model.repositoryContext = repositoryContext;

  }

  /**
   * Unit
   * Get a model that is created
   */
  @Test
  void getCreatedModel() {
    createAndRegisterRepository();
    ModelImplementationTest modelImplementationTest = new ModelImplementationTest();
    try {
      modelImplementationTest.create();
      assertTrue(new ModelImplementationTest(modelImplementationTest.getId()).get().isPresent());
    } catch (RepositoryNotFoundException e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      fail();
    }
  }

  /**
   * Unit
   * Get an model that is not created
   */
  @Test
  void getEmptyModel() {
    createAndRegisterRepository();
    try {
      assertFalse(new ModelImplementationTest(0).get().isPresent());
    } catch (RepositoryNotFoundException e) {
      e.printStackTrace();
      System.out.println(e.getMessage());
      fail();
    }
  }

  /**
   * Unit
   * Get an model that when Repository is not registered
   */
  @Test
  void unregisterRepo() {
    Model.repositoryContext = new RepositoryContext();
    ModelImplementationTest modelImplementationTest = new ModelImplementationTest();
    Assertions.assertThrows(RepositoryNotFoundException.class, modelImplementationTest::create);

  }

  void createAndRegisterRepository() {
    ModelImplementationRepositoryTest modelImplementationRepositoryTest = new ModelImplementationRepositoryTest();
    repositoryContext
        .registerRepository(ModelImplementationTest.class, modelImplementationRepositoryTest);
  }
}
