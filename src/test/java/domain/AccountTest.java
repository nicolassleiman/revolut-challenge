package domain;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;

import com.revolut.challenge.domain.Account;
import java.util.Objects;
import org.junit.jupiter.api.Test;

/**
 * Testing the Account Model
 */
class AccountTest {


  /**
   * Unit
   * Create an account using its repository
   */
  @Test
  void creation() {
    Account account = new Account(0L, 10L);
    assertTrue(account.getId().equals(0l)
        && account.getBalance().equals(10l)
        && account.getInbound() != null
        && account.getOutbound() != null);
  }

  /**
   * Unit
   * Deposit an account using its repository
   */
  @Test
  void deposit() {
    Account account = new Account(0L, 10L);
    account.deposit(10L);
    assertEquals(20L, (long) account.getBalance());
  }

  /**
   * Unit
   * Withdraw from an account using its
   */
  @Test
  void withdraw() {
    Account account = new Account(0L, 10L);
    account.withdraw(10L);
    assertEquals(0L, (long) account.getBalance());
  }

  /**
   * Unit
   * Test the equals function
   */
  @Test
  void structuralEquality() {
    Account account = new Account(0L, 0L);
    Account test = new Account(0L, 0L);
    assertEquals(account, test);
  }

  /**
   * Unit
   * Test the string representation
   */
  @Test
  void toStringTest() {
    Account account = new Account(0L, 0L);
    String test = "Account{" +
        "balance=0, inbound=" + account.getInbound() +
        ", outbound=" + account.getOutbound() +
        ", id=0}";
    assertEquals(account.toString(), test);
  }

  /**
   * Unit
   * Test the hashing function
   */
  @Test
  void hashing() {
    Account account = new Account(0L, 0L);
    int hashcode = Objects
        .hash(account.getId(), account.getBalance(), account.getInbound(), account.getOutbound());
    assertEquals(hashcode, account.hashCode());
  }


}
