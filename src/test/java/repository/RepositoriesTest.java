package repository;

import static junit.framework.TestCase.assertTrue;

import com.revolut.challenge.configuration.RepositoryContext;
import com.revolut.challenge.repositories.Repository;
import domain.ModelImplementationTest;
import org.junit.jupiter.api.Test;

class RepositoriesTest {

  /**
   * Unit
   * Creates a repository and registers it
   */
  @Test
  public void registerRepository() {
    RepositoryContext context = new RepositoryContext();
    Repository<ModelImplementationTest> repository = new ModelImplementationRepositoryTest();
    ModelImplementationTest modelImplementationTest = new ModelImplementationTest();
    context.registerRepository(ModelImplementationTest.class, repository);
    assertTrue(context.getRepository(ModelImplementationTest.class).isPresent()
        && context.getRepository(ModelImplementationTest.class).get() == repository);
  }
}
