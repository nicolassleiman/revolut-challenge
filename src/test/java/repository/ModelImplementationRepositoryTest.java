package repository;

import com.revolut.challenge.repositories.Repository;
import domain.ModelImplementationTest;

/**
 * Dummy Model Repository to use in the model implementation
 */
public class ModelImplementationRepositoryTest extends Repository<ModelImplementationTest> {

}
