package resources;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

import com.revolut.challenge.configuration.RepositoryContext;
import com.revolut.challenge.domain.Account;
import com.revolut.challenge.domain.Model;
import com.revolut.challenge.domain.Transfer;
import com.revolut.challenge.exceptions.EntityContainsIdException;
import com.revolut.challenge.exceptions.EntityNotFoundException;
import com.revolut.challenge.exceptions.FailedToMapException;
import com.revolut.challenge.exceptions.RepositoryNotFoundException;
import com.revolut.challenge.repositories.AccountRepository;
import com.revolut.challenge.repositories.TransferRepository;
import com.revolut.challenge.resources.AccountResource;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Unit & Integration test for the AccountResource
 */
class AccountResourceIntTest {

  private static RepositoryContext repositoryContext;
  private static AccountResource accountResource;


  /**
   * Creates the context and registers the repositories
   */
  @BeforeAll
  static void createContext() {
    repositoryContext = new RepositoryContext();
    accountResource = new AccountResource();
    Model.repositoryContext = repositoryContext;
    repositoryContext.registerRepository(Account.class, new AccountRepository());
    repositoryContext.registerRepository(Transfer.class, new TransferRepository());
  }

  /**
   * Unit:
   * Create a new account using the resource
   * Create operation should sucees with 200 response status
   */
  @Test
  void post() {
    Account account = new Account(null);
    try {
      Response response = accountResource.createAccount(account);
      assertEquals(200, response.getStatus());
    } catch (RepositoryNotFoundException | EntityContainsIdException | FailedToMapException e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * Unit
   * Tries to post when no repository has been registered
   * Create operation should fail, with exception thrown
   */
  @Test
  void postNoRepository() {
    Model.repositoryContext = new RepositoryContext(); // emptying the context
    Account account = new Account(null);
    Assertions.assertThrows(RepositoryNotFoundException.class,
        () -> accountResource.createAccount(account));
  }

  /**
   * Unit
   * Tries to post when id is already
   * Create operation should fail, with exception thrown
   */
  @Test
  void postWithId() {
    Account account = new Account(1L);
    Assertions.assertThrows(EntityContainsIdException.class,
        () -> accountResource.createAccount(account));
  }

  /**
   * Unit
   * Tries to get an account
   * Get operation should succeed
   */
  @Test
  void get() {
    try {
      Account account = new Account(null).create();
      assertEquals(account, accountResource.getAccount(account.getId()).getEntity());
    } catch (EntityNotFoundException | RepositoryNotFoundException e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * Unit
   * Tries to get an object when no repository has been registered
   * Get operation should fail, with exception thrown
   */
  @Test
  void getNoRepository() {
    Model.repositoryContext = new RepositoryContext(); // empty the context
    // The get methods throws the RepositoryNotFoundException before checking for the EntityNotFoundException
    Assertions
        .assertThrows(RepositoryNotFoundException.class, () -> accountResource.getAccount(0L));
  }

  /**
   * Unit
   * Tries to get an object that is not registered
   * Get operation should fail, with exception thrown
   */
  @Test
  void getWithUnkownId() {
    Assertions.assertThrows(EntityNotFoundException.class, () -> accountResource.getAccount(1L));
  }

  /**
   * Unit
   * Tries to add money inside an account that is already created
   * Put operation should succeed and balance updated
   */
  @Test
  void addMoney() {
    try {
      Long id = null;
      Account account = new Account(id, 0L).create();
      AccountResource accountResource = new AccountResource();
      accountResource.deposit(account.getId(), 10L);
      assertEquals(10L, (long) account.getBalance());
    } catch (RepositoryNotFoundException | EntityNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * Unit
   * Tries to add money inside an account when no repository has been registered
   * Put operation should fail, with exception thrown
   */
  @Test
  void addMoneyRepositoryNotFound() {
    Model.repositoryContext = new RepositoryContext(); // emptying the context
    AccountResource accountResource = new AccountResource();
    // The get methods throws the RepositoryNotFoundException before checking for the EntityNotFoundException
    Assertions.assertThrows(RepositoryNotFoundException.class,
        () -> accountResource.deposit(0L, 10L));

  }

  /**
   * Unit
   * Tries to add money inside an account that is not registered
   * Put operation should fail, with exception thrown
   */
  @Test
  void addMoneyEntityNotFound() {
    AccountResource accountResource = new AccountResource();
    Assertions.assertThrows(EntityNotFoundException.class,
        () -> accountResource.deposit(0L, 10L));

  }

}
