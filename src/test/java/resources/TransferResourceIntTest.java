package resources;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

import com.revolut.challenge.configuration.RepositoryContext;
import com.revolut.challenge.domain.Account;
import com.revolut.challenge.domain.Model;
import com.revolut.challenge.domain.Transfer;
import com.revolut.challenge.exceptions.EntityContainsIdException;
import com.revolut.challenge.exceptions.FailedToMapException;
import com.revolut.challenge.exceptions.ForeignKeyNotFoundException;
import com.revolut.challenge.exceptions.NotEnoughFundsException;
import com.revolut.challenge.exceptions.RepositoryNotFoundException;
import com.revolut.challenge.repositories.AccountRepository;
import com.revolut.challenge.repositories.TransferRepository;
import com.revolut.challenge.resources.AccountResource;
import com.revolut.challenge.resources.TransferResource;
import java.time.Instant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TransferResourceIntTest {

  private static RepositoryContext repositoryContext;
  private static AccountResource accountResource;

  /**
   * Creates the context and registers the repositories
   */
  @BeforeAll
  static void createContext() {
    repositoryContext = new RepositoryContext();
    accountResource = new AccountResource();
    Model.repositoryContext = repositoryContext;
    repositoryContext.registerRepository(Account.class, new AccountRepository());
    repositoryContext.registerRepository(Transfer.class, new TransferRepository());
  }

  /**
   * Unit
   * Transfer money between two account
   * Post operation should suceed
   */
  @Test
  void transfer() {
    try {
      createContext();
      Account account = new Account(null).create();
      Account account1 = new Account(null).create();

      account.deposit(100L);

      Transfer transfer = new Transfer(null, account.getId(), account1.getId(), 20L,
          "test", Instant.now());
      TransferResource transferResource = new TransferResource();
      transferResource.createTransfer(transfer);

      assertTrue(account.getOutbound().contains(transfer));
      assertTrue(account1.getInbound().contains(transfer));

    } catch (RepositoryNotFoundException | EntityContainsIdException | ForeignKeyNotFoundException | NotEnoughFundsException | FailedToMapException e) {
      e.printStackTrace();
      fail();
    }
  }

  /**
   * Unit
   * Transfer money between two account when no repository has been registered
   * Post operation should fail, with exception thrown
   */
  @Test
  public void transferNoRepository() {
    createContext();
    Account account = null;
    Account account1 = null;
    try {
      account = new Account(null).create();
      account1 = new Account(null).create();
    } catch (RepositoryNotFoundException e) {
      e.printStackTrace();
    }
    account.deposit(100L);
    Model.repositoryContext = new RepositoryContext();
    Transfer transfer = new Transfer(null, account.getId(), account1.getId(), 20L,
        "test", Instant.now());
    TransferResource transferResource = new TransferResource();
    Assertions.assertThrows(RepositoryNotFoundException.class,
        () -> transferResource.createTransfer(transfer));

  }


  /**
   * Unit
   * Transfer money between two account, one of them does not exist
   * Post operation should fail, with exception thrown
   */
  @Test
  public void transferForeignKeyViolation() {

    Account account = null;
    Account account1 = null;
    Model.repositoryContext = repositoryContext;
    try {
      account = new Account(null).create();
      account1 = new Account(null).create();
    } catch (RepositoryNotFoundException e) {
      e.printStackTrace();
    }
    account.deposit(100L);
    Transfer transfer = new Transfer(null, account.getId(), account1.getId() + 1, 20L,
        "test", Instant.now());
    TransferResource transferResource = new TransferResource();
    Assertions.assertThrows(ForeignKeyNotFoundException.class,
        () -> transferResource.createTransfer(transfer));

  }

  /**
   * Unit
   * Transfer money between two account with not enough funds
   * Post operation should fail, with exception thrown
   */
  @Test
  public void transferNotEnoughFunds() {

    Account account = null;
    Account account1 = null;
    try {
      account = new Account(null).create();
      account1 = new Account(null).create();
    } catch (RepositoryNotFoundException e) {
      e.printStackTrace();
    }
    account.deposit(100L);
    Transfer transfer = new Transfer(null, account.getId(), account1.getId(), 120L,
        "test", Instant.now());
    TransferResource transferResource = new TransferResource();
    Assertions.assertThrows(NotEnoughFundsException.class,
        () -> transferResource.createTransfer(transfer));

  }


  /**
   * Unit
   * Transfer money between two account when the posted entity already contains the id
   * Post operation should fail, with exception thrown
   */
  @Test
  public void transferContainsId() {

    Account account = null;
    Account account1 = null;
    try {
      account = new Account(null).create();
      account1 = new Account(null).create();
    } catch (RepositoryNotFoundException e) {
      e.printStackTrace();
    }
    account.deposit(100L);
    Transfer transfer = new Transfer(10L, account.getId(), account1.getId()+1, 120L,
        "test", Instant.now());
    TransferResource transferResource = new TransferResource();
    Assertions.assertThrows(EntityContainsIdException.class,
        () -> transferResource.createTransfer(transfer));

  }

}
