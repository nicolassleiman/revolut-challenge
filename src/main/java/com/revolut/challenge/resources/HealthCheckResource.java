package com.revolut.challenge.resources;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST controller for transferring money between accounts
 */
@Path("/v1/api/healthchecks/")
@Produces(MediaType.APPLICATION_JSON)
public class HealthCheckResource {

  @GET
  @Path("/up")
  public Response isUp() {
    ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
    objectNode.put("status", "up");
    return Response.ok(objectNode).build();
  }
}
