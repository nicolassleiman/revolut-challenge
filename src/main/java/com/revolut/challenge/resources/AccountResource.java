package com.revolut.challenge.resources;

import com.codahale.metrics.annotation.Timed;
import com.revolut.challenge.domain.Account;
import com.revolut.challenge.exceptions.EntityContainsIdException;
import com.revolut.challenge.exceptions.EntityNotFoundException;
import com.revolut.challenge.exceptions.FailedToMapException;
import com.revolut.challenge.exceptions.RepositoryNotFoundException;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST controller for managing an account
 */
@Path("/v1/api/accounts/")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

  private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

  /**
   * GET /accounts/{id} Gets the account given and id and returns it
   *
   * @param id The id of the account we want to query
   * @return A response object containing the the object
   * @throws EntityNotFoundException if the id does not match any account entity
   * @throws RepositoryNotFoundException if the repository for the model is not registered
   */
  @GET
  @Timed
  @Path("{id}")
  public Response getAccount(@PathParam("id") Long id)
      throws EntityNotFoundException, RepositoryNotFoundException {
    logger.debug("GET:accounts/{id} getAccount() entered with arguments: [" + id + "]");
    Optional<Account> account = new Account(id).get();

    if (!account.isPresent()) {
      throw new EntityNotFoundException();
    }

    return Response.ok(account.get()).build();
  }

  /**
   * POST /accounts creates An account with the post body
   *
   * @param account The account's data
   * @return A response entity containing the created account
   * @throws RepositoryNotFoundException if the repository for the model is not registered
   * @throws EntityContainsIdException if the entity already contains an id
   */
  @POST
  @Timed
  @Consumes({MediaType.APPLICATION_JSON})
  public Response createAccount(Account account)
      throws RepositoryNotFoundException, EntityContainsIdException, FailedToMapException {
    logger.debug(
        "POST:accounts/ createAccount() entered with arguments: [" + account.toString() + "]");
    if(account == null) throw new FailedToMapException();
    if (account.getId() != null) throw new EntityContainsIdException();


    return Response.ok(account.create()).build();
  }

  /**
   * PUT accounts/{id}/deposit/{amount}
   *
   * @param id the id of the account in which we should deposit
   * @param amount the amount to deposit
   * @return a response entity containing the updated account
   * @throws EntityNotFoundException if the id does not match an account entity
   * @throws RepositoryNotFoundException if the repository for the model is not registered
   */
  @PUT
  @Timed
  @Path("{id}/deposit/{amount}")
  @Consumes({MediaType.APPLICATION_JSON})
  public Response deposit(@PathParam("id") Long id, @PathParam("amount") Long amount)
      throws EntityNotFoundException, RepositoryNotFoundException {
    logger.debug("PUT:accounts/ deposit() entered with arguments: [" + id + ", " + amount + "]");
    Optional<Account> optionalAccount = new Account(id).get();
    if (!optionalAccount.isPresent()) {
      throw new EntityNotFoundException();
    }

    Account account = optionalAccount.get();
    account.deposit(amount);
    return Response.ok(account).build();
  }
}
