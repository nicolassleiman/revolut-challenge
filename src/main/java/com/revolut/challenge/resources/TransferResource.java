package com.revolut.challenge.resources;

import com.codahale.metrics.annotation.Timed;
import com.revolut.challenge.domain.Account;
import com.revolut.challenge.domain.Transfer;
import com.revolut.challenge.exceptions.EntityContainsIdException;
import com.revolut.challenge.exceptions.FailedToMapException;
import com.revolut.challenge.exceptions.ForeignKeyNotFoundException;
import com.revolut.challenge.exceptions.NotEnoughFundsException;
import com.revolut.challenge.exceptions.RepositoryNotFoundException;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * REST controller for transferring money between accounts
 */
@Path("/v1/api/transfers/")
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

  private static final Logger logger = LoggerFactory.getLogger(TransferResource.class);

  /**
   * POST: Creates a transfer between accounts
   *
   * @param transfer the transfer object
   * @return a response containing the transfer object with its id
   * @throws ForeignKeyNotFoundException if one or more account does not exist
   * @throws RepositoryNotFoundException if the repository is not registered
   * @throws EntityContainsIdException if the transfer entity already has an id
   * @throws NotEnoughFundsException if the outbound account does not have enough funds to make the
   * transfer
   */
  @POST
  @Timed
  @Consumes({MediaType.APPLICATION_JSON})
  public Response createTransfer(Transfer transfer) throws ForeignKeyNotFoundException,
      RepositoryNotFoundException, EntityContainsIdException, NotEnoughFundsException,
      FailedToMapException {
    if(transfer == null) throw new FailedToMapException();
    logger.debug("POST:transfers/ createTransfer() entered with arguments: [" + transfer + "]");
    if (transfer.getId() != null) {
      throw new EntityContainsIdException();
    }
    Optional<Account> from = new Account(transfer.getFromId()).get();
    Optional<Account> to = new Account(transfer.getToId()).get();
    if (from.isEmpty() || to.isEmpty()) {
      throw new ForeignKeyNotFoundException();
    }
    if (from.get().getBalance() < transfer.getAmount()) {
      throw new NotEnoughFundsException();
    }
    Transfer createdTransfer = transfer.create();
    from.get().withdraw(transfer.getAmount());
    from.get().addOutbound(transfer);
    to.get().deposit(transfer.getAmount());
    to.get().addInbound(transfer);
    from.get().update();
    to.get().update();

    return Response.ok(createdTransfer).build();
  }
}
