package com.revolut.challenge.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.time.Instant;
import java.util.Objects;

public class Transfer extends Model<Transfer> {

  /**
   * From and to id can be external (iban, account number or sort code). But for this example, to
   * actually transfer money between account, it has to match an account id in the system
   */
  private Long fromId;
  private Long toId;
  private Long amount;
  private String reference;
  private Instant timestamp;


  /**
   * Constructor with all fields
   *
   * @param id the id for the transfer object
   * @param fromId the account from which the money will be withdrawn
   * @param toId the account in which the money will be deposited
   * @param amount the amount to be transferred
   * @param reference the reference of the transfer
   * @param timestamp the timestamp
   */
  public Transfer(Long id, Long fromId, Long toId, Long amount, String reference,
      Instant timestamp) {
    super(id);
    this.fromId = fromId;
    this.toId = toId;
    this.amount = amount;
    this.reference = reference;
    this.timestamp = timestamp;
  }

  /**
   * Constructor used by Jackson json to construct a Transfer object
   *
   * @param id the id for the transfer object
   * @param fromId the account from which the money will be withdrawn
   * @param toId the account in which the money will be deposited
   * @param amount the amount to be transferred
   * @param reference the reference of the transfer
   */
  @JsonCreator
  public Transfer(@JsonProperty("id") Long id, @JsonProperty("fromId") Long fromId,
      @JsonProperty("toId") Long toId, @JsonProperty("amount") Long amount,
      @JsonProperty("reference") String reference) {
    super(id);
    this.fromId = fromId;
    this.toId = toId;
    this.amount = amount;
    this.reference = reference;
    this.timestamp = Instant.now();
  }

  /**
   * @return the id of the account from which the money will be withdrawn
   */
  @JsonProperty("fromId")
  public Long getFromId() {
    return fromId;
  }

  /**
   * @return the account in which the money will be deposited
   */
  @JsonProperty("toId")
  public Long getToId() {
    return toId;
  }

  /**
   * @return the amount to be transferred
   */
  @JsonProperty("amount")
  public Long getAmount() {
    return amount;
  }


  @Override
  public String toString() {
    return "Transfer{" +
        "fromId=" + fromId +
        ", toId=" + toId +
        ", amount=" + amount +
        ", id=" + id +
        ", reference=" + reference +
        ", timestamp=" + timestamp +
        '}';
  }

  @JsonProperty("reference")
  public String getReference() {
    return reference;
  }

  @JsonProperty("timestamp")
  @JsonSerialize(using = ToStringSerializer.class)
  public Instant getTimestamp() {
    return timestamp;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transfer transfer = (Transfer) o;
    return Objects.equals(fromId, transfer.fromId) &&
        Objects.equals(toId, transfer.toId) &&
        Objects.equals(amount, transfer.amount) &&
        Objects.equals(reference, transfer.reference) &&
        Objects.equals(timestamp, transfer.timestamp);
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), fromId, toId, amount, reference, timestamp);
  }
}
