package com.revolut.challenge.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.revolut.challenge.configuration.RepositoryContext;
import com.revolut.challenge.exceptions.RepositoryNotFoundException;
import com.revolut.challenge.repositories.Repository;
import java.util.Optional;

public class Model<T extends Model<T>> {

  @JsonIgnore
  public static RepositoryContext repositoryContext;
  protected Long id;


  public Model() {

  }

  public Model(Long id) {
    this.id = id;
  }

  /**
   * @return the model with id being
   */
  @JsonIgnore
  public Optional<T> get() throws RepositoryNotFoundException {
    return getRepository().get(this.id);
  }

  /**
   * Unchecked cast is fine as the extension of Model extends itself Synchronized because it uses
   * critical section
   *
   * @return the created model
   */
  @JsonIgnore
  public synchronized T create() throws RepositoryNotFoundException {
    return getRepository().create((T) this);
  }


  /**
   * Unchecked cast is fine as the extension of Model extends itself Synchronized
   *
   * @return the created model
   */
  @JsonIgnore
  public synchronized T update() throws RepositoryNotFoundException {
    return getRepository().update((T) this);
  }

  /**
   * @return the repositories
   * @throws RepositoryNotFoundException if the Model repositories is not registered
   */
  @JsonIgnore
  protected Repository<T> getRepository() throws RepositoryNotFoundException {
    Optional<Repository> wrappedRepo = repositoryContext.getRepository(getClass());
    if (wrappedRepo.isPresent()) {
      return wrappedRepo.get();
    } else {
      throw new RepositoryNotFoundException();
    }
  }

  /**
   * @return the model's id
   */
  @JsonProperty("id")
  public Long getId() {
    return id;
  }

  /**
   * Set the id of the model
   *
   * @param id the id of the model
   * @return a reference to the object itself for chainc commands
   */
  @JsonProperty("id")
  public T setId(long id) {
    this.id = id;
    return (T) this;
  }
}
