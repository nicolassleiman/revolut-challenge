package com.revolut.challenge.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * The representation of an Account
 */
public class Account extends Model<Account> {

  private Long balance;

  @JsonIgnore
  private List<Transfer> inbound;

  @JsonIgnore
  private List<Transfer> outbound;

  /**
   * Constructs an account using the id only, useful for using querying an account
   *
   * @param id the id of the account
   */
  public Account(Long id) {
    super(id);
    balance = 0L;
    inbound = new ArrayList<>();
    outbound = new ArrayList<>();
  }

  /**
   * Constructor used by Jackson json to construct an Account object
   *
   * @param id the id of the account
   * @param balance the balance of the account
   */
  @JsonCreator
  public Account(@JsonProperty("id") Long id, @JsonProperty("balance") Long balance) {
    this.id = id;
    this.balance = balance;
    inbound = new ArrayList<>();
    outbound = new ArrayList<>();
  }

  /**
   * Deposit money into the account: synchronized (concurrent modification)
   *
   * @param amount the amount to be added to the account
   */
  public synchronized void deposit(long amount) {
    this.balance += amount;
  }

  /**
   * Withdraw money from the account: synchronized (concurrent modification)
   *
   * @param amount the amount to be removed to the account
   */
  public synchronized void withdraw(long amount) {
    this.balance -= amount;
  }

  /**
   * @return the balance of he account
   */
  public Long getBalance() {
    return balance;
  }

  /**
   * @return the transfers that went in the account
   */
  @JsonProperty("inbound")
  public List<Transfer> getInbound() {
    return inbound;
  }

  /**
   * @return the transfers that went in the account
   */
  @JsonProperty("outbound")
  public List<Transfer> getOutbound() {
    return outbound;
  }

  /**
   *
   */
  public synchronized void addInbound(Transfer transfer) {
    inbound.add(transfer);
  }

  /**
   *
   */
  public synchronized void addOutbound(Transfer transfer) {
    outbound.add(transfer);
  }

  @Override
  public String toString() {
    return "Account{" +
        "balance=" + balance +
        ", inbound=" + inbound +
        ", outbound=" + outbound +
        ", id=" + id +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Account account = (Account) o;
    return Objects.equals(balance, account.balance) &&
        Objects.equals(inbound, account.inbound) &&
        Objects.equals(outbound, account.outbound);
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId(), balance, inbound, outbound);
  }
}

