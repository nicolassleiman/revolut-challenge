package com.revolut.challenge.repositories;

import com.revolut.challenge.domain.Account;

/**
 * Repository holding the Account Model objects. Custom functions go into this class.
 */
public class AccountRepository extends Repository<Account> {

}
