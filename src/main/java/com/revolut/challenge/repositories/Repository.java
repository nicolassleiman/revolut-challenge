package com.revolut.challenge.repositories;

import com.revolut.challenge.domain.Model;
import java.util.HashMap;
import java.util.Optional;

/**
 * Abstract class containing basic Model querying operation
 *
 * @param <T> the Model of the repository
 */
public class Repository<T extends Model> {

  private static long ID = 0;
  private HashMap<Long, T> modelHashMap = new HashMap<>();

  /**
   * Get's the model given an id
   *
   * @param id the id of the Model
   * @return A null optional if the id does not exist, else containing the Model
   */
  public Optional<T> get(long id) {
    return Optional.ofNullable(modelHashMap.getOrDefault(id, null));
  }

  /**
   * Creates a model and registers it in datastore
   *
   * @param m the model to register
   * @return the model with the id
   */
  public synchronized T create(T m) {
    modelHashMap.put(ID, (T) m.setId(ID++));
    return m;
  }

  /**
   * Updates a model and saving in the database
   *
   * @param m the model to update
   * @return the updated model with the id
   */
  public synchronized T update(T m) {
    modelHashMap.put(ID, (T) m.setId(ID++));
    return m;
  }
}
