package com.revolut.challenge.repositories;

import com.revolut.challenge.domain.Transfer;

/**
 * Repository holding the Transfer Model objects. Custom functions go into this class.
 */
public class TransferRepository extends Repository<Transfer> {

}

