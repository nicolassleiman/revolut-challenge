package com.revolut.challenge.exceptions.mappers;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.revolut.challenge.exceptions.ResourceException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class ResourceExceptionMapper implements ExceptionMapper<ResourceException> {

  @Override
  public Response toResponse(ResourceException e) {
    ObjectNode objectNode = JsonNodeFactory.instance.objectNode();
    objectNode.put("exception", e.getMessage());
    return Response.status(500)
        .entity(objectNode)
        .type(MediaType.APPLICATION_JSON)
        .build();
  }
}
