package com.revolut.challenge.exceptions;

/**
 * Thrown when an entity does not have  repository
 */
public class RepositoryNotFoundException extends ResourceException {

  @Override
  public String getMessage() {
    return "No repositories found for class:" + getStackTrace()[0].getClass();
  }
}
