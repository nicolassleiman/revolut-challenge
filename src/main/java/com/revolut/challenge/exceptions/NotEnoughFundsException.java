package com.revolut.challenge.exceptions;

/**
 * Thrown when an account does not have enough funds to do the requested operation
 */
public class NotEnoughFundsException extends ResourceException {

  @Override
  public String getMessage() {
    return "The account does not have enough funds to transfer";
  }
}
