package com.revolut.challenge.exceptions;

/**
 * Thrown when a foreign key constraint is violated
 */
public class ForeignKeyNotFoundException extends ResourceException {

  @Override
  public String getMessage() {
    return "Constraint violation: One or more foreign keys are not valid";
  }
}
