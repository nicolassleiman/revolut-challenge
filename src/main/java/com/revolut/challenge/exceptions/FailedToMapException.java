package com.revolut.challenge.exceptions;

public class FailedToMapException extends ResourceException {
  @Override
  public String getMessage() {
    return "Mapping failed for entity";
  }
}
