package com.revolut.challenge.exceptions;

/**
 * Thrown when an entity is posted with the id
 */
public class EntityContainsIdException extends ResourceException {

  @Override
  public String getMessage() {
    return "An entity id cannot be specified on creation";
  }
}
