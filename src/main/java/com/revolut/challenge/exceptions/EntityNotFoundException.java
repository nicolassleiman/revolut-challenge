package com.revolut.challenge.exceptions;

/**
 * Thrown when the id did not match an entity
 */
public class EntityNotFoundException extends ResourceException {

  @Override
  public String getMessage() {
    return "No Entity found for given id";
  }
}
