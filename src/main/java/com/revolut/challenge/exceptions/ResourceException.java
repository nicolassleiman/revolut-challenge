package com.revolut.challenge.exceptions;

public class ResourceException extends Throwable {

  public final int CODE = 500;
}
