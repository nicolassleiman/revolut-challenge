package com.revolut.challenge;

import com.codahale.metrics.health.HealthCheck;
import com.revolut.challenge.configuration.RepositoryContext;
import com.revolut.challenge.configuration.TransferConfiguration;
import com.revolut.challenge.domain.Account;
import com.revolut.challenge.domain.Model;
import com.revolut.challenge.domain.Transfer;
import com.revolut.challenge.exceptions.mappers.ResourceExceptionMapper;
import com.revolut.challenge.healthchecks.DummyHealthCheck;
import com.revolut.challenge.repositories.AccountRepository;
import com.revolut.challenge.repositories.Repository;
import com.revolut.challenge.repositories.TransferRepository;
import com.revolut.challenge.resources.AccountResource;
import com.revolut.challenge.resources.HealthCheckResource;
import com.revolut.challenge.resources.TransferResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import java.util.Arrays;
import javax.ws.rs.ext.ExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransferApplication extends Application<TransferConfiguration> {

  private static final Logger logger = LoggerFactory.getLogger(TransferApplication.class);
  private static final Object[] resources = new Object[]{
      new AccountResource(),
      new TransferResource(),
      new HealthCheckResource()
  };
  private static final ExceptionMapper[] mappers = new ExceptionMapper[]{
      new JsonProcessingExceptionMapper(true),
      new ResourceExceptionMapper()
  };
  private static final Entry[] checks = new Entry[]{
      new Entry<>("HealtCheck", new DummyHealthCheck())
  };
  private static final Entry[] repositories = new Entry[]{
      new Entry<>(Account.class, new AccountRepository()),
      new Entry<>(Transfer.class, new TransferRepository())
  };
  private static RepositoryContext context;

  /**
   * Main function: runs the application
   * @param args Dropwizard application arguments
   * @throws Exception if Dropwizard fails
   */
  public static void main(String[] args) throws Exception {
    logger.info("Starting application with arguments: " + Arrays.toString(args));
    new TransferApplication().run(args);
  }

  /**
   * Register the resources into the jersey environment
   * @param environment jersey environment for the Dropwizard application
   */
  private void registerResources(Environment environment) {
    logger.debug("Registering resources");
    for (Object o : resources) {
      environment.jersey().register(o);
    }
  }

  /**
   * Register the custom exceptions into the jersey environment
   * @param environment jersey environment for the Dropwizard application
   */
  private void registerExceptions(Environment environment) {
    logger.debug("Registering exceptions");
    for (ExceptionMapper m : mappers) {
      environment.jersey().register(m);
    }
  }


  /**
   * Register the custom exceptions into the Dropwizard environment
   * @param environment jersey environment for the Dropwizard application
   */
  private void registerHealthChecks(Environment environment) {
    logger.debug("Registering health checks");
    for (Entry check : checks) {
      if (check.getK() instanceof String && check.getV() instanceof HealthCheck) {
        environment.healthChecks().register((String) check.getK(), (HealthCheck) check.getV());
      }
    }
  }

  /**
   * Register the repositories for the model into the Repositories Context and defines the context
   * in the Model class
   */
  private void registerRepositories() {
    logger.debug("Registering repositories");
    context = new RepositoryContext();
    Model.repositoryContext = context;
    for (Entry r : repositories) {
      if (r.getK() instanceof Class && r.getV() instanceof Repository) {
        context.registerRepository((Class) r.getK(), (Repository) r.getV());
      }
    }
  }

  /**
   * Starts the Dropwizard application
   * Registers:
   * <ul>
   *   <li>repositories</li>
   *   <li>resources</li>
   *   <li>exceptions</li>
   *   <li>health checks</li>
   * </ul>
   * @param transferConfiguration
   * @param environment
   */
  public void run(TransferConfiguration transferConfiguration, Environment environment) {
    String appName = transferConfiguration.getApplication().getName();

    long start = System.currentTimeMillis();

    registerRepositories();
    registerResources(environment);
    registerExceptions(environment);
    registerHealthChecks(environment);

    long end = System.currentTimeMillis();
    logger.info(appName + " application started in: " + (end - start) + "ms");
  }

  /**
   * Initialize the application with a custom resource configuration provider
   * @param bootstrap the Transfer configurations
   */
  @Override
  public void initialize(Bootstrap<TransferConfiguration> bootstrap) {
    logger.debug("Initializing configuration source provider");
    bootstrap.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());
  }

  /**
   * Key Value data entry
   * @param <K> key
   * @param <V> value
   */
  private static class Entry<K, V> {

    private K k;
    private V v;

    Entry(K k, V v) {
      this.k = k;
      this.v = v;
    }

    K getK() {
      return k;
    }

    V getV() {
      return v;
    }

  }
}
