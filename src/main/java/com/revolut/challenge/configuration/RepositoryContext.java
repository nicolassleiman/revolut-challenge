package com.revolut.challenge.configuration;

import com.revolut.challenge.repositories.Repository;
import java.util.HashMap;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Context for the repositories.
 */
public class RepositoryContext {

  private static final Logger logger = LoggerFactory.getLogger(Repository.class);

  private HashMap<Class, Repository> repositoryMap = new HashMap<>();

  /**
   * Register a repository for a Model
   *
   * @param model the Model class you want to register the repository
   * @param repository the repository
   */
  public void registerRepository(Class model, Repository repository) {
    logger.debug("Registering repository for class: " + model);
    repositoryMap.put(model, repository);
  }

  /**
   * Gets the repository for the class
   *
   * @param model the class model for which we want the repository
   * @return the repository
   */
  public Optional<Repository> getRepository(Class model) {
    return Optional.ofNullable(repositoryMap.getOrDefault(model, null));
  }
}
