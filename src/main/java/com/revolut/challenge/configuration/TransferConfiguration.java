package com.revolut.challenge.configuration;


import io.dropwizard.Configuration;

/**
 * The configuration class for the Dropwizard applicaiton
 */
public class TransferConfiguration extends Configuration {

  private Application application;

  public Application getApplication() {
    return application;
  }

  public void setApplication(Application application) {
    this.application = application;
  }

  public static class Application {

    private String name;

    public String getName() {
      return name;
    }

    public void setName(String applicationName) {
      this.name = applicationName;
    }
  }
}
