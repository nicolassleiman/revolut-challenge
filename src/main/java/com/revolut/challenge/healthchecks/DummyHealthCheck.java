package com.revolut.challenge.healthchecks;

import com.codahale.metrics.health.HealthCheck;

/**
 * Dummy Health check, replace with useful checks (DB, registry, cache)
 */
public class DummyHealthCheck extends HealthCheck {

  public DummyHealthCheck(){

  }

  @Override
  protected Result check() throws Exception {
    return Result.healthy();

  }
}
