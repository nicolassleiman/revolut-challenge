<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://maven.apache.org/POM/4.0.0"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <artifactId>challenge</artifactId>

  <build>
    <plugins>
      <plugin>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>${maven.surfire.version}</version>
      </plugin>
      <plugin>
        <artifactId>maven-failsafe-plugin</artifactId>
        <version>${maven.failsafe.version}</version>
      </plugin>
      <plugin>
        <!-- Build an executable JAR -->
        <artifactId>maven-jar-plugin</artifactId>
        <configuration>

          <archive>
            <manifest>
              <addClasspath>true</addClasspath>
              <classpathPrefix>lib/</classpathPrefix>
              <mainClass>com.revolut.challenge.TransferApplication</mainClass>
            </manifest>
          </archive>

        </configuration>
        <groupId>org.apache.maven.plugins</groupId>
        <version>${maven.jar.version}</version>
      </plugin>
      <plugin>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <source>11</source>
          <target>11</target>
        </configuration>
        <groupId>org.apache.maven.plugins</groupId>
        <version>3.1</version>
      </plugin>
      <plugin>
        <artifactId>maven-shade-plugin</artifactId>
        <executions>
          <execution>
            <configuration>
              <transformers>
                <transformer
                  implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <mainClass>com.revolut.challenge.TransferApplication</mainClass>
                </transformer>
                <transformer
                  implementation="org.apache.maven.plugins.shade.resource.ServicesResourceTransformer">
                </transformer>
              </transformers>
            </configuration>
            <goals>
              <goal>shade</goal>
            </goals>
            <phase>package</phase>
          </execution>
        </executions>
        <groupId>org.apache.maven.plugins</groupId>
        <version>${maven.shade.version}</version>
      </plugin>
      <plugin>
        <artifactId>jacoco-maven-plugin</artifactId>
        <configuration>
          <append>true</append>
          <output>file</output>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
            <id>jacoco-initialize</id>
          </execution>
          <execution>
            <goals>
              <goal>report</goal>
            </goals>
            <id>jacoco-site</id>
            <phase>verify</phase>
          </execution>
          <!-- The Executions required by unit tests are omitted. -->
          <!--
              Prepares the property pointing to the JaCoCo runtime agent which
              is passed as VM argument when Maven the Failsafe plugin is executed.
          -->
          <execution>
            <configuration>
              <!-- Sets the path to the file which contains the execution data. -->
              <destFile>${project.build.directory}/coverage-reports/jacoco-it.exec</destFile>
              <!--
                  Sets the name of the property containing the settings
                  for JaCoCo runtime agent.
              -->
              <propertyName>failsafeArgLine</propertyName>
            </configuration>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
            <id>pre-integration-test</id>
            <phase>pre-integration-test</phase>
          </execution>
          <!--
              Ensures that the code coverage report for integration tests after
              integration tests have been run.
          -->
          <execution>
            <configuration>
              <!-- Sets the path to the file which contains the execution data. -->
              <dataFile>${project.build.directory}/coverage-reports/jacoco-it.exec</dataFile>
              <!-- Sets the output directory for the code coverage report. -->
              <outputDirectory>${project.reporting.outputDirectory}/jacoco-it</outputDirectory>
            </configuration>
            <goals>
              <goal>report</goal>
            </goals>
            <id>post-integration-test</id>
            <phase>post-integration-test</phase>
          </execution>
        </executions>
        <groupId>org.jacoco</groupId>
        <version>${jacoco.version}</version>

      </plugin>
    </plugins>
  </build>
  <dependencies>
    <!-- https://mvnrepository.com/artifact/io.dropwizard/dropwizard-core -->
    <dependency>
      <artifactId>dropwizard-core</artifactId>
      <groupId>io.dropwizard</groupId>
      <version>${dropwizard.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/junit/junit -->
    <dependency>
      <artifactId>junit-jupiter-engine</artifactId>
      <groupId>org.junit.jupiter</groupId>
      <scope>test</scope>
      <version>${junit.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin -->
    <dependency>
      <artifactId>jacoco-maven-plugin</artifactId>
      <groupId>org.jacoco</groupId>
      <version>${jacoco.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-api -->
    <dependency>
      <artifactId>slf4j-api</artifactId>
      <groupId>org.slf4j</groupId>
      <version>${sl4j.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/ch.qos.logback/logback-classic -->
    <dependency>
      <artifactId>logback-classic</artifactId>
      <groupId>ch.qos.logback</groupId>
      <scope>runtime</scope>
      <version>${logback.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.slf4j/jcl-over-slf4j -->
    <dependency>
      <artifactId>jcl-over-slf4j</artifactId>
      <groupId>org.slf4j</groupId>
      <version>${sl4j.over.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/com.mashape.unirest/unirest-java -->
    <dependency>
      <groupId>com.mashape.unirest</groupId>
      <artifactId>unirest-java</artifactId>
      <version>${unirest.version}</version>
    </dependency>
    <!-- TEST -->
    <!-- https://mvnrepository.com/artifact/io.dropwizard/dropwizard-testing -->
    <dependency>
      <groupId>io.dropwizard</groupId>
      <artifactId>dropwizard-testing</artifactId>
      <version>${dropwizard.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
  <groupId>com.revolut</groupId>
  <modelVersion>4.0.0</modelVersion>
  <packaging>jar</packaging>

  <properties>
    <dropwizard.version>2.0.0-rc9</dropwizard.version>
    <jacoco.version>0.8.4</jacoco.version>
    <junit.version>5.5.1</junit.version>
    <logback.version>1.2.3</logback.version>
    <maven.compiler.source>11</maven.compiler.source>
    <maven.compiler.target>11</maven.compiler.target>
    <maven.failsafe.version>2.22.2</maven.failsafe.version>
    <maven.jar.version>3.1.2</maven.jar.version>
    <maven.shade.version>3.2.1</maven.shade.version>
    <maven.surfire.version>2.22.2</maven.surfire.version>
    <sl4j.over.version>1.7.28</sl4j.over.version>
    <sl4j.version>1.7.28</sl4j.version>
    <unirest.version>1.4.9</unirest.version>
  </properties>


  <version>1.0-SNAPSHOT</version>
</project>